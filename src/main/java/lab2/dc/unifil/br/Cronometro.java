package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Write a description of class lab2.dc.unifil.br.Cronometro here.
 * 
 * @author Ricardo Inacio Alvares e Silva
 * @version 20200519
 */
public class Cronometro {

    /**
     * Faz benchmarkings do algoritmo que recebe uma entrada N, inicialmente igual a
     * nInicial, até nFinal, incrementando a nPasso. Retorna uma lista com todas
     * essas medições, classificada do menor N ao maior.
     *
     * @param nInicial          Valor escalar de N inicialmente.
     * @param nFinal            Valor escalar de N que interrompe as medições.
     * @param nPasso            Quantidade de incremento em N a cada iteração de
     *                          medições.
     * @param repeticoes        Quantidade de vezes que cada medição é feita. Quanto
     *                          maior o valor, mais precisa a medição, mas mais
     *                          demorado o processo total.
     * @param fabricadorCobaias Método que cria estruturas de dados aceitas como
     *                          entrada pelo algoritmo em teste, com dimensão
     *                          configurável para permitir o benchmarking em
     *                          crescimento.
     * 
     * @param algoritmo         Algoritmo a ser testado.
     * @param <T>               Tipo da estrutura de dados do algoritmo.
     * @return Lista com medições de tempo, classificada do menor ao maior N.
     */
    public static <T> List<Medicao> benchmarkCrescimentoAlgoritmo(int nInicial, int nFinal, int nPasso, int repeticoes,
            Function<Integer, T> fabricadorCobaias, Consumer<T> algoritmo) {

        List<Medicao> listaMedicoes = new ArrayList<Medicao>();

        // while (nInicial < nFinal) {

        // listaMedicoes.add(new Medicao(nMedicao,
        // benchmarkAlgoritmo(fabricadorCobaias.apply(nInicial), algoritmo,
        // repeticoes)));
        // nMedicao++;
        // nInicial = nInicial + nPasso;
        // }

        for (int i = nInicial; i < nFinal; i += nPasso) {
            final int tamanho = i;
            Supplier<T> sp = () -> fabricadorCobaias.apply(tamanho);

            listaMedicoes.add(new Medicao(i, benchmarkAlgoritmo(sp, algoritmo, repeticoes)));
        }

        return listaMedicoes;
    }

    /**
     * O aluno deve escrever um javadocs adequado para esse método.
     */

    public static <T> double benchmarkAlgoritmo(Supplier<T> cobaia, Consumer<T> algoritmo, int repeticoes) {
        Cronometro cron = new Cronometro();
        double menorTempo = Double.POSITIVE_INFINITY;
        for (int i = 0; i < repeticoes; i++) {

            cron.zerar();
            cron.iniciar();
            T lista = cobaia.get();
            algoritmo.accept(lista);
            double ultimoTempo = cron.parar();

            menorTempo = Math.min(menorTempo, ultimoTempo);
        }

        return menorTempo;
    }

    /**
     * Construtor padrão da classe.
     */
    public Cronometro() {

        tempoInicial = 0;
        tempoFinal = 0;
        tempoDecorrido = 0;
        isPausado = true;
    }

    /**
     * Inicia ou reinicia a contagem de tempo. Nunca zera o último estado do
     * contador. Se o tempo já estiver correndo, não faz nada.
     */
    public void iniciar() {
        if (isPausado) {
            isPausado = false;
            tempoInicial = System.nanoTime();
        }

    }

    /**
     * Para a contagem de tempo e retorna uma leitura do tempo decorrido.
     * 
     * @return Tempo decorrido até o momento da parada, em milissegundos.
     */
    public double parar() {
        tempoFinal = System.nanoTime();
        tempoDecorrido += tempoFinal - tempoInicial;
        isPausado = true;
        return tempoDecorrido;

    }

    /**
     * Retorna o tempo decorrido contado até então, independente se está parado ou
     * correndo. Não altera o estado de contagem (parado/correndo).
     * 
     * @return Tempo decorrido contado pelo cronômetro, em milissegundos.
     */
    public double lerTempoEmMilissegundos() {

        double tempoParcial = System.nanoTime();
        return tempoDecorrido + (tempoParcial - tempoInicial);
    }

    /**
     * Zera o contador de tempo do cronômetro. Se o cronômetro estava em estado de
     * contagem, ele é parado.
     */
    public void zerar() {

        tempoInicial = 0;
        tempoFinal = 0;
        tempoDecorrido = 0;
        isPausado = true;
    }

    // Atributos da classe são declarados aqui
    double tempoInicial;
    double tempoFinal;
    double tempoDecorrido;
    boolean isPausado;
}
