package lab2.dc.unifil.br;

import java.util.List;
import java.util.Random;

public class Classificadores {

    public static void bogosort(List<Integer> lista) {
        while (!isOrdenada(lista))
            permutar(lista, rng.nextInt(lista.size()), rng.nextInt(lista.size()));
    }

    private static void permutar(List<Integer> lista, int a, int b) {
        Integer permutador = lista.get(a);
        lista.set(a, lista.get(b));
        lista.set(b, permutador);
    }

    private static boolean isOrdenada(List<Integer> lista) {
        for (int i = 1; i < lista.size(); i++)
            if (lista.get(i - 1) > lista.get(i))
                return false;

        return true;
    }

    static int HoarePartition(List<Integer> lista, int low, int high) {
        int pivot = lista.get(low);
        int i = low - 1, j = high + 1;

        while (true) {
            do {
                i++;
            } while (lista.get(i) < pivot);
            do {
                j--;
            } while (lista.get(j) > pivot);

            // If two pointers met.
            if (i >= j)
                return j;
            int temp = lista.get(i);
            lista.set(i, lista.get(j));
            lista.set(j, temp);

        }
    }

    /*
     * The main function that implements QuickSort arr[] --> Array to be sorted, low
     * --> Starting index, high --> Ending index
     */

    static void quickSort(List<Integer> lista) {
        quickSort(lista, 0, lista.size() - 1);
    }

    static void quickSort(List<Integer> lista, int low, int high) {
        if (low < high) {
            int pi = HoarePartition(lista, low, high);
            quickSort(lista, low, pi);
            quickSort(lista, pi + 1, high);
        }
    }

    static void selectionSort(final List<Integer> lista) {
        for (int i = 0; i < lista.size() - 1; i++) {
            int minElementIndex = i;
            for (int j = i + 1; j < lista.size(); j++) {
                if (lista.get(minElementIndex) > lista.get(j)) {

                    minElementIndex = j;
                }
            }
            if (minElementIndex != i) {
                int temp = lista.get(i);
                lista.set(i, lista.get(minElementIndex));
                lista.set(minElementIndex, lista.get(temp));
            }
        }
    }

    /*
     * https://github.com/eugenp/tutorials/blob/
     * 16086d8221478977809ead26f48ce6dd52c992f5/algorithms-sorting/src/main/java/com
     * /baeldung/algorithms/bubblesort/BubbleSort.java
     */
    static void optimizedBubbleSort(List<Integer> lista) {
        int i = 0, n = lista.size();

        boolean swapNeeded = true;
        while (i < n - 1 && swapNeeded) {
            swapNeeded = false;
            for (int j = 1; j < n - i; j++) {
                if (lista.get(j - 1) > lista.get(j)) {

                    int temp = lista.get(j - 1);
                    lista.set(j - 1, lista.get(j));
                    lista.set(j, lista.get(temp));
                    swapNeeded = true;
                }
            }
            if (!swapNeeded)
                break;
            i++;
        }
    }

    public static void insertionSortRecursive(List<Integer> lista) {
        insertionSortRecursive(lista, lista.size() - 1);
    }

    /**
     * https://github.com/eugenp/tutorials/blob/16086d8221478977809ead26f48ce6dd52c992f5/algorithms-sorting/src/main/java/com/baeldung/algorithms/insertionsort/InsertionSort.java
     */
    private static void insertionSortRecursive(List<Integer> lista, int i) {
        // base case
        if (i <= 1) {
            return;
        }
        // sort the first i - 1 elements of the array
        insertionSortRecursive(lista, i - 1);

        // then find the correct position of the element at position i
        int key = lista.get(i - 1);
        int j = i - 2;
        // shifting the elements from their position by 1
        while (j >= 0 && lista.get(j) > key) {
            lista.set(j + 1, lista.get(j));
            j = j - 1;
        }
        // inserting the key at the appropriate position
        lista.set(j + 1, key);
    }

    private static Random rng = new Random("Seed constante repetível".hashCode());
}
