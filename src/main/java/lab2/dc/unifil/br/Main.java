package lab2.dc.unifil.br;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

        public static void main(String[] args) {
                final Integer nInicial = 10;
                final Integer nFinal = 2000;
                final Integer nPasso = 100;
                final Integer repeticoes = 100;

                List<Medicao> medicoesBogosort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso,
                                repeticoes, FabricaListas::fabricarListaIntegersCrescente, Classificadores::bogosort);

                List<Medicao> medicoesQuickDecre = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso,
                                repeticoes, FabricaListas::fabricarListaIntegersDecrescente,
                                Classificadores::quickSort);

                List<Medicao> medicoesQuick = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso,
                                repeticoes, FabricaListas::fabricarListaIntegersCrescente, Classificadores::quickSort);

                List<Medicao> medicoesSelectionSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso,
                                repeticoes, FabricaListas::fabricarListaIntegersCrescente,
                                Classificadores::selectionSort);

                List<Medicao> medicoesBubleSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso,
                                repeticoes, FabricaListas::fabricarListaIntegersCrescente,
                                Classificadores::optimizedBubbleSort);
                List<Medicao> medicoesInsertionSort = Cronometro.benchmarkCrescimentoAlgoritmo(nInicial, nFinal, nPasso,
                                repeticoes, FabricaListas::fabricarListaIntegersCrescente,
                                Classificadores::insertionSortRecursive);

                // INICIA A MONTAR A ESTRUTURA DE DADOS DO GRAFICO QUE SERA PLOTADO
                TabelaTempos tt = new TabelaTempos();
                tt.setTitulo("Tempo para ordenação");
                tt.setEtiquetaX("Qtde elementos lista");
                tt.setEtiquetaY("Tempo (s)");

                tt.setLegendas("bubleSort", "insertionSort");

                // TODO VERIFICAR QUAL O TAMANHO CORRETO DO FOR
                for (int i = 0; i < medicoesQuickDecre.size(); i++) {
                        // Medicao amostraQuickDecre = medicoesQuickDecre.get(i);
                        // Medicao amostraQuick = medicoesQuick.get(i);
                        // Medicao amostraBogo = medicoesBogosort.get(i);
                        // Medicao amostraSelection = medicoesSelectionSort.get(i);
                        Medicao amostraBuble = medicoesBubleSort.get(i);
                        Medicao amostraInsertion = medicoesInsertionSort.get(i);

                        // AMOSTRA PARA SEREM PLOTADAS
                        tt.anotarAmostra(amostraBuble.getN(),
                                        // amostraQuickDecre.getTempoSegundos(),
                                        // amostraQuick.getTempoSegundos(),
                                        // amostraBogo.getTempoSegundos(),
                                        amostraBuble.getTempoSegundos(), amostraInsertion.getTempoSegundos()

                        );
                }
                tt.exibirGraficoXY();

        }

}
